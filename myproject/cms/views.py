from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Contenido

# Create your views here.
formulario="""
Esta llave no tiene valor asignado.
<p>
Asignale un valor:
<p>
<form action="" method ="POST">
    Valor: <input type="text" name="valor">
    </br><input type="submit" value="Enviar">
    
</form>
"""


def index(request):
    return HttpResponse("Hola estas en la pagina principal de la app CMS")

@csrf_exempt
def get_content(request, llave):
    if request.method=="PUT":
        valor = request.PUT['valor']
        c = Contenido(clave=llave, valor=valor)
        c.save()
    if request.method=="POST":
        valor = request.POST['valor']
        c = Contenido(clave=llave, valor=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = ("La llave " + llave + " tiene como valor: " + contenido.valor)
    except Contenido.DoesNotExist:
        respuesta = formulario
    return HttpResponse(respuesta)